const configuration = {
  financingRate: 2.8,
  hitch: 10,
  maximumPaymentTerms: 12
};

const articles = [
  {
    _id: '59dec7342c3ce821fbabb5b1',
    description: 'Macbook Pro 2019',
    model: 'MIA1',
    price: 4250,
    existence: 10
  },
  {
    _id: '59dec7342c3ce821fbabb5b2',
    description: 'iPhone X',
    model: 'SQMIFS',
    price: 20000,
    existence: 100
  },
  {
    _id: '59dec7342c3ce821fbabb5b3',
    description: 'Escritorio Cristal',
    model: 'ZD7MI',
    price: 1000,
    existence: 2
  },
  {
    _id: '59dec7342c3ce821fbabb5b4',
    description: 'Labadora Wirpool',
    model: 'D9FW',
    price: 3500,
    existence: 15
  },
  {
    _id: '59dec7342c3ce821fbabb5b5',
    description: 'Macbook Pro 2016',
    model: '2S00J',
    price: 15000,
    existence: 20
  }
];

const clients = [
  {
    _id: '59dd61fa850175d53ae03cf1',
    name: 'Kiri',
    fatherLastName: 'Rodriguez',
    motherLastName: 'Bond',
    rfc: 'S98DS98ASD89D98'
  },
  {
    _id: '59dd61fa850175d53ae03cf2',
    name: 'David',
    fatherLastName: 'Miranda',
    motherLastName: 'Lopez',
    rfc: '0990DFSDFSDF5'
  },
  {
    _id: '59dd61fa850175d53ae03cf3',
    name: 'Mario',
    fatherLastName: 'Escamilla',
    motherLastName: 'Fernandez',
    rfc: 'ASDKMALSDMASD'
  },
  {
    _id: '59dd61fa850175d53ae03cf4',
    name: 'Kenia',
    fatherLastName: 'Felix',
    motherLastName: 'Rendon',
    rfc: 'QEOPEPOQWEOPQOPWE'
  },
  {
    _id: '59dd61fa850175d53ae03cf5',
    name: 'Karina',
    fatherLastName: 'Evans',
    motherLastName: 'Hensword',
    rfc: 'AKLSDKLASLKDAKLSD'
  },
  {
    _id: '59dd61fa850175d53ae03cf6',
    name: 'Melissa',
    fatherLastName: 'Valenzuela',
    motherLastName: 'Rodriguez',
    rfc: 'UIGFIUFGIUFIUG3'
  },
  {
    _id: '59dd61fa850175d53ae03cf7',
    name: 'Felipe',
    fatherLastName: 'Quintero',
    motherLastName: 'Bond',
    rfc: 'NMNB3N3BN3NBNB4'
  },
];

module.exports = {
  configuration: configuration,
  articles: articles,
  clients: clients
};
