import shortid from 'shortid';
import _ from 'lodash';
import busboy from 'koa-busboy';

export function imageMiddleware () {
  return busboy({
    dest: './cdn',
    fnDestFilename: (fieldname, filename) => {
      console.log('imageMiddleware');
      console.log('fieldname:',fieldname);
      console.log('filename:',filename);
      const nameParts = filename.split('.');
      const fileType = _.last(nameParts);
      return shortid.generate() + '.' + fileType;
    }
  });
}
