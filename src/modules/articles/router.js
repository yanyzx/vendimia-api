import * as article from './controller'

export const baseUrl = '/articles'

export default [
  {
    method: 'GET',
    route: '/',
    handlers: [
      article.getArticles
    ],
    permissions: '*'
  },
  {
    method: 'GET',
    route: '/:saleId',
    handlers: [
      article.getById
    ],
    permissions: '*'
  }
]
