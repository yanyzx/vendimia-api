import _ from 'lodash';
import to from 'await-to';
import Bang from 'bang';
import config from '../../../config'

export async function getClients (ctx) {
  const Client = ctx.app.models.client;
  const { query } = ctx.query;

  const { data: clients, err } = await to(Client.find());
  if (err) throw Bang.wrap(err);

  clients.forEach(client => {
    client.shownName = client.id + ' - ' + client.name + ' ' +
                       client.fatherLastName + ' ' + client.motherLastName;
  });

  if (query) {
    var regex = new RegExp(query, 'i')

    const filteredClients = _.filter(clients, (client) => {
      const valid = regex.test(client.shownName);
      return valid;
    });

    ctx.body = filteredClients;
  }
  else {
    ctx.body = clients;
  }
}

export async function getById (ctx) {
  const Client = ctx.app.models.client;
  const clientId = ctx.params.clientId;

  const { data: client, err } = await to(Client.findById(clientId));
  if (!client) throw Bang.notFound('client with provided id does not exist');
  if (err) throw Bang.wrap(err);

  ctx.body = client;
}
