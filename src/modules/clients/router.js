import * as client from './controller'

export const baseUrl = '/clients'

export default [
  {
    method: 'GET',
    route: '/',
    handlers: [
      client.getClients
    ],
    permissions: '*'
  },
  {
    method: 'GET',
    route: '/:saleId',
    handlers: [
      client.getById
    ],
    permissions: '*'
  }
]
