import * as configuration from './controller'

export const baseUrl = '/configurations'

export default [
  {
    method: 'POST',
    route: '/',
    handlers: [
      configuration.createConfiguration
    ],
    permissions: '*'
  },
  {
    method: 'GET',
    route: '/findOne',
    handlers: [
      configuration.getConfiguration
    ],
    permissions: '*'
  }
]
